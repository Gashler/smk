Thank you for your order with StockMusicKing.com. You can download your music library by clicking on the following link:

{{ env('APP_URL') }}/download/{{ $data['token'] }}

Your order ID is {{ $data['order']->id }}.
