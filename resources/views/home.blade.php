@extends('master')
@section('content')
<div ng-app="app" ng-controller="HomeController">
    <div id="header">
        <img id="logo" src="/img/logo-white.png" alt="StockMusicKing.com">
    </div>
    <section id="home-2">
        <div id="home-2-1">
            <h2>Instantly Download a Complete Music Library</h2>
            <p>Super bulk savings on fantastic sounds for all of your films, media, or video games</p>
        </div>
        <div id="home-2-2">
            <div ng-click="setTrack('Symphonic')">
                <img ng-class="{ 'active': trackCategory == 'Symphonic' }" src="/img/home/symphonic.jpg" alt="symphonic">
                <h3>Symphonic</h3>
            </div>
            <div ng-click="setTrack('Jazz')">
                <img ng-class="{ 'active': trackCategory == 'Jazz' }" src="/img/home/jazz.jpg" alt="jazz">
                <h3>Jazz</h3>
            </div>
            <div ng-click="setTrack('Folk')">
                <img ng-class="{ 'active': trackCategory == 'Folk' }" src="/img/home/folk.jpg" alt="folk">
                <h3>Folk</h3>
            </div>
            <div ng-click="setTrack('Pop')">
                <img ng-class="{ 'active': trackCategory == 'Pop' }" src="/img/home/pop.jpg" alt="pop">
                <h3>Pop</h3>
            </div>
            <div ng-click="setTrack('Rock')">
                <img ng-class="{ 'active': trackCategory == 'Rock' }" src="/img/home/rock.jpg" alt="rock">
                <h3>Rock</h3>
            </div>
            <div ng-click="setTrack('World')">
                <img ng-class="{ 'active': trackCategory == 'World' }" src="/img/home/world.jpg" alt="world">
                <h3>World</h3>
            </div>
        </div>
        <div ng-show="" ng-include=""></div>
        <div id="audioPlayer" class="audioPlayer" class="align-center" ng-cloak>
            <br>
            <div class="controls" ng-class="{ 'disabled': playing && !audio.buffered.length }">
                <div class="control-btn" ng-click="playPause()"><i class="fa" ng-class="{ 'fa-pause': playing, 'fa-play': !playing }"></i></div>
                <div class="control-btn" ng-click="setTrack()"><i class="fa fa-fast-forward"></i></div>
                <div class="progress-container">
                    <div class="clickable" ng-click="setTrack(null, trackIndex, $event);"></div>
                    <div class="progress" style="width: @{{ (audio.currentTime / audio.duration) * 100 }}%"></div>
                </div>
            </div>
            {{-- <h3>@{{ trackCategory }}</h3> --}}
            <ol class="playlist">
                <li
                    ng-repeat="track in tracks[trackCategory]"
                    ng-class="{ 'active': $index == trackIndex }"
                    ng-click="setTrack(null, $index);"
                >
                    {{-- <div class="progress" style="width: @{{ (audio.currentTime / audio.duration) * 100 }}%"></div> --}}
                    <div class="content">
                        <img ng-show="playing && audio.currentTime == 0 && !audio.buffered.length && $index == trackIndex" src="/img/loading2.gif" width="20" height="20" class="loading">
                        <span class="name">@{{ ($index + 1) + '. &nbsp;' + track.title }}</span>
                        <span class="duration">@{{ track.duration }}</span>
                        <span class="stars" style="width: @{{ track.rating * 16 }}px"></span>
                    </div>
                    <div class="clickable"></div>
                </li>
            </ol>
        </div>
        <div class="align-center" id="scroll-icon">
            <i class="fa fa-arrows-v margin"></i>
        </div>
    </section>
    <section id="home-1" class="flex">
        <div id="home-1-1" class="col-6">
            <img id="home-1-img" src="/img/home/1.jpg">
            <div class="position-relative">
                <h1><img src="/img/home/home-1-1-b.png" alt="StockMusicKing.com - Over 60 tracks of high-quality stock music for only $99 (90% off)"></h1>
                <br>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/gDcB0wMTdxY?rel=0&amp;showinfo=0&autoplay=0" frameborder="0" allowfullscreen></iframe>
            </div>
            <div class="black-filter"></div>
        </div>
        <div id="home-1-2" class="col-6"></div>
    </section>
    <section class="flex" id="home-5">
        <div id="home-5-1">
            <h2>Compare the Value</h2>
            <table>
                <thead>
                    <tr>
                        <th>Company</th>
                        <th>Price per Track</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>PremiumBeat.com</td>
                        <td>$49.00</td>
                    </tr>
                    <tr>
                        <td>Shutterstock.com</td>
                        <td>$49.00</td>
                    </tr>

                    <tr>
                        <td>StockMusic.net</td>
                        <td>$39.95</td>
                    </tr>
                    <tr class="highlight">
                        <td>StockMusicKing.com</td>
                        <td>$1.99</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </section>
    <section id="home-3" class="flex">
        <div class="col-6" id="home-3-1">
            <div id="home-3-1-1" class="inline-block align-left">
                <img class="img-left" src="/img/home/stephen-gashler.jpg" alt="Stephen Gashler">
                Created by national<br>
                award-winning artist<br>
                Stephen Gashler
            </div>
            <br>
            <br>
            <div class="inline-block">
                <quote>
                    "I love this man's music!"
                </quote>
                <cite>
                    -- Tina Rayley<br>
                    Executive Producer<br>
                    Proper Manors
                </cite>
            </div>
            <br>
            <br>
            <div class="inline-block">
                <quote>
                    "Just what I was looking for"
                </quote>
                <cite>
                    -- Randall McNair<br>
                    Filmmaker<br>
                </cite>
            </div>
        </div>
        <div class="col-6" id="home-3-2">
            <div class="inline-block">
                <h2>100% Royalty Free</h2>
                <ul class="align-left">
                    <li>Professionaly mixed and mastered</li>
                    <li>High fidelity WAV + MP3 files</li>
                    <li>Unlimited usage rights</li>
                    <li>Instant digital download</li>
                    <li>Satisfaction Guaranteed</li>
                </ul>
            </div>
        </div>
    </section>
    <section class="flex" id="home-4">
        <div class="col-6" id="home-4-1">
            <div id="home-4-1-1">
                <h2>#1 Best Value for Stock Music</h2>
                <p>
                    Whether you're looking for soothing background music or an epic score, StockMusicKing.com has got you covered. Enjoy a rich library of high quality, professionally-mastered music in many different genres at a fraction of the market price, including songs nominated for best soundtracks. Not only is this music 100% royalty free, but you can use it without retriction in as many projects as you’d like.
                </p>
                <p>
                    We're confident that you'll love this fantastic music library. If you’re not fully satisfied, simply contact us within 30 days to recieve a full refund, no questions asked.
                </p>
            </div>
        </div>
        <div class="col-6" id="home-4-2">
            <div id="home-4-2-1">
                <p>Only @{{ price / 100 | currency }} for Over 60 tracks of high quality stock music</p>
                <p>Instant Download <i class="fa fa-download"></i></p>
                <script src="https://checkout.stripe.com/checkout.js"></script>
                <form ng-show="allowCoupon" ng-submit="applyCoupon()">
                    <div class="input-group margin-auto">
                        <input type="text" ng-model="code" placeholder="Promotional Code (optional)">
                        <button>Apply</button>
                    </div>
                    <br>
                </form>
                <div ng-if="coupon && coupon.success" class="alert success">
                    Discount Applied: @{{ coupon.message }}<br>
                    Your Price: only @{{ coupon.price / 100 | currency }}
                </div>
                <div ng-if="coupon && !coupon.success" class="alert danger">
                    @{{ coupon.message }}
                </div>
                <button id="customButton" ng-click="cancelOffer(); progress('click-buy-now')" class="btn btn-primary">Buy Now <i class="semitransparent fa fa-credit-card"></i></button>
                <br>
                <img src="/img/home/home-4-badges.png" alt="90% Off, Satisfaction Guarantee" style="vertical-align: middle;">
                <img src="/img/home/comodo_secure_seal_113x59_transp.png" alt="Comodo Secure" style="vertical-align: middle; max-width: 113px;">
            </div>
        </div>
    </section>
    <div class="popup" id="special-offer">
        <div class="panel">
            <div class="panel-heading">
                <h2>Today's Special Offer - 50% Off</h2>
            </div>
            <div class="panel-body">
                <p>When you buy today, download the entire StockMusicKing.com library for only <strong><div class="strike">$99.00<div class="line"></div></div> $49.50</strong></p>
            </div>
            <div class="panel-footer">
                <button class="btn btn-default" ng-click="progress('reject-offer'); hidePopup('#special-offer')">No Thanks</button>
                <button class="btn btn-primary" ng-click="progress('accept-offer'); applyCoupon('halfoffbaby')">Apply Coupon</button>
            </div>
        </div>
    </div>
</div>
<script>




    /******************
    * Stripe Functions
    *******************/

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var handler = StripeCheckout.configure({
      key: '{{ env('STRIPE_KEY') }}',
      locale: 'auto',
      token: function(token) {
        // You can access the token ID with `token.id`.
        // Get the token ID to your server-side code for use.
      }
    });

    document.getElementById('customButton').addEventListener('click', function(e) {
      // Open Checkout with further options:
      handler.open({
        amount: price,
        allowRememberMe: false,
        description: "Purchase stock music library",
        // panelLabel: '',
        // zipCode: true,
        // billingAddress: true,
        token: function(response) {
            $('body').html('<img src="/img/loading.gif" style="max-width:434px;">').css('background', 'rgb(20,20,20)');
            if (response.id) {
                $.post('/process-order', {
                    email: response.email,
                    token: response.id,
                    card: response.card.id
                }, function() {
                    window.location = '/download/' + response.id;
                });
            }
        }
      });
      e.preventDefault();
    });

    // Close Checkout on page navigation:
    window.addEventListener('popstate', function() {
        handler.close();
    });

    @if (session('price'))
        var price = {{ session('price') }};
    @else
        var price = {{ config('site.price') }};
    @endif
</script>
<script src="/js/controllers/HomeController.js"></script>
@stop
