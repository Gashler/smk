<?php

use App\Order;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/download/{token}', function ($token) {
    $order = Order::where('token', $token)->first();
    if ($order || $token = 'QloXliONDJvTuarTgOgH'/* free download */) {
        return view('download');
    }
});

// progress
Route::get('progress/{eventName}', 'ProgressController@store');

Route::get('/apply-coupon/{code}', function ($code) {
    $price = config('site.price');
    $result = [
        'success' => false,
        'message' => "Promotional code not found.",
        'price' => $price
    ];
    if ($code == 'halfoffbaby') {
        $result = [
            'success' => true,
            'message' => "50% Off",
            'price' => round($price /= 2)
        ];
    }
    if ($code == 'friendofsteve') {
        $result = [
            'success' => true,
            'message' => "100% Off",
            'price' => 0
        ];
    }
    session(['price' => $result['price']]);
    return $result;
});

Route::post('/process-order', function() {
    $data = request()->all();


    // Set your secret key: remember to change this to your live secret key in production
    // See your keys here: https://dashboard.stripe.com/account/apikeys
    \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

    // Token is created using Stripe.js or Checkout!
    // Get the payment token submitted by the form:

    // Charge the user's card:
    $price = session('price');
    if (!$price) {
        $price = config('site.price');
    }
    $charge = \Stripe\Charge::create(array(
      "amount" => $price,
      "currency" => "usd",
      "description" => "Stock music library",
      "source" => $data['token'],
    ));


    $data['order'] = Order::create($data);
    // Mail::send('email_confirmation', ['data' => $data], function ($m) use ($data) {
    //     $m->from('stockmusicking@gmail.com', 'StockMusicKing.com');
    //     $m->to($data['email'])->subject('Your download is ready');
    //     return view('download');
    // });

    /*********** MAIL US *************/

    // $to = "americanknight@gmail.com";
    // $subject = "New StockMusicKing.com Customer";
    // $message = "
    // <html>
    //     <head>
    //         <title>New StockMusicKing.com Customer</title>
    //     </head>
    //     <body>
    //         <h1>New StockMusicKing.com Customer</h1>
    //         <table>
    // ";
    // foreach ($data as $key => $value) {
    //     $message .= "
    //             <tr>
    //                 <th style='text-align:right;'>" . ucfirst($key) . "</th>
    //                 <td>" . $value . "</td>
    //             </tr>
    //     ";
    // }
    // $message .= "
    //         </table>
    //     </body>
    // </html>
    // ";
    //
    // // Always set content-type when sending HTML email
    // $headers = "MIME-Version: 1.0\r\n";
    // $headers .= "Content-type:text/html;charset=UTF-8\r\n";
    //
    // // More headers
    // $headers .= "From: <stockmusicking@gmail.com>";
    //
    // mail($to, $subject, $message, $headers);


    /***** MAIL CUSTOMER ******/


    $to = $data['email'];
    $subject = "Thank you for your order";
    $message = "
<p>Thank you for your order with StockMusicKing.com. You can download your music library by clicking on the following link:</p>

<p>".env('APP_URL')."./download/". $data['token'] ."</p>

<p>Your order ID is ". $data['order']->id ."</p>
";

    // Always set content-type when sending HTML email
    $headers = "MIME-Version: 1.0\r\n";
    $headers .= "Content-type:text/html;charset=UTF-8\r\n";
    $headers .= "Bcc: <americanknight@gmail.com>\r\n";
    $headers .= "From: StockMusicKing.com <noreply@stockmusicking.com>";

    mail($to, $subject, $message, $headers);
});
