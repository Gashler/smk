// home controller
var app = angular.module('app', []);
angular.module('app').controller('HomeController', function($scope, $http, $interval) {

    $scope.tracks = {
        'Folk': [
            {
                mp3:'/mp3/I%20am%20the%20Man%20You%20Loved.mp3',
                title:"I Am the Man You Loved",
                artist:'Stephen Gashler, Curtis Wiederhold',
                rating:5,
                duration:'2:05'
            },
            {
                mp3:'/mp3/Shackles.mp3',
                title:"Shackles",
                artist:'Stephen Gashler, Curtis Wiederhold',
                rating:5,
                duration:'2:34'
            },
            {
                mp3:'/mp3/The%20Man%20on%20the%20Hill.mp3',
                title:"The Man on the Hill",
                artist:'Stephen Gashler',
                rating:5,
                duration:'3:22'
            },
            {
                mp3:'/mp3/The%20Sacrifice.mp3',
                title:"The Sacrifice",
                artist:'Stephen Gashler',
                rating:5,
                duration:'2:53'
            },
            {
                mp3:'/mp3/Kristoff%20Cures%20it%20All.mp3',
                title:"Kristoff Cures it All",
                artist:'Stephen Gashler',
                rating:4,
                duration:'2:10'
            },
            {
                mp3:'/mp3/Waltz%20with%20the%20Devil.mp3',
                title:"Waltz With the Devil",
                artist:'Stephen and Teresa Gashler',
                rating:4,
                duration:'3:22'
            },
            {
                mp3:'/mp3/Falling%20Apart.mp3',
                title:"Falling Apart",
                artist:'Stephen Gashler, Curtis Wiederhold',
                rating:4,
                duration:'3:19'
            },
            {
                mp3:'/mp3/We%20Only%20Live%20to%20Die.mp3',
                title:"We Only Live to Die",
                artist:'Stephen Gashler',
                rating:4,
                duration:'5:14'
            },
            {
                mp3:'/mp3/Brutes%20Throwing%20Rocks%20at%20Each%20Other.mp3',
                title:"Brutes Throwing Rocks",
                artist:'Stephen Gashler',
                rating:3,
                duration:'1:24'
            },
            {
                mp3:'/mp3/Old%20Daniel.mp3',
                title:"Old Daniel",
                artist:'Stephen Gashler',
                rating:3,
                duration:'3:43'
            },
        ],
        'Symphonic': [
            {
                mp3:'/mp3/The%20Book%20of%20Life.mp3',
                title:"The Book of Life",
                artist:'Stephen Gashler',
                rating:5,
                duration:'4:19'
            },
            {
                mp3:'/mp3/Queen%20of%20the%20Flies.mp3',
                title:"Queen of the Flies",
                artist:'Stephen Gashler',
                rating:5,
                duration:'4:01'
            },
            {
                mp3:'/mp3/Valhalla.mp3',
                title:"Valhalla",
                artist:'Stephen Gashler',
                rating:5,
                duration:'6:09'
            },
            {
                mp3:'/mp3/Super%20Goat.mp3',
                title:"Super Goat",
                artist:'Stephen Gashler',
                rating:5,
                duration:'2:21'
            },
            {
                mp3:'/mp3/The%20Devil%20Went%20Down%20to%20Deleware.mp3',
                title:"The Devil Went Down to Delaware",
                artist:'Stephen Gashler',
                rating:4,
                duration:'3:24'
            },
            {
                mp3:'/mp3/We%27ll%20Go%20to%20Valhalla.mp3',
                title:"We'll Go to Valhalla",
                artist:'Stephen Gashler',
                rating:4,
                duration:'4:37'
            },
            {
                mp3:'/mp3/Edward%20Versus%20the%20World.mp3',
                title:"Edward Versus the World",
                artist:'Stephen Gashler',
                rating:4,
                duration:'4:18'
            },
            {
                mp3:'/mp3/The%20Corporate%20Gods.mp3',
                title:"The Corporate Gods",
                artist:'Stephen Gashler',
                rating:4,
                duration:'3:43'
            },
            {
                mp3:'/mp3/The%20Corporate%20Gods%20Reprise.mp3',
                title:"The Corporate Gods Reprise",
                artist:'Stephen Gashler',
                rating:3,
                duration:'1:42'
            },
            {
                mp3:'/mp3/Rhubarbara%27s%20Dream.mp3',
                title:"Rhubarbara's Dream",
                artist:'Stephen Gashler',
                rating:3,
                duration:'0:42'
            },
            {
                mp3:'/mp3/Edward%20and%20Rhubarbara%27s%20Dream.mp3',
                title:"Edward and Rhubarbara's Dream",
                artist:'Stephen Gashler',
                rating:3,
                duration:'2:20'
            }
        ],
        'Jazz': [
            {
                mp3:'/mp3/Hopeless%20Romantic.mp3',
                title:"Hopeless Romantic",
                artist:'Stephen Gashler',
                rating:5,
                duration:'3:50'
            },
            {
                mp3:'/mp3/Spaz.mp3',
                title:"Spaz",
                artist:'Stephen Gashler',
                rating:5,
                duration:'1:26'
            },
            {
                mp3:'/mp3/En%27tracte.mp3',
                title:"En'tracte",
                artist:'Stephen Gashler',
                rating:5,
                duration:'4:36'
            },
            {
                mp3:'/mp3/Boring%2C%20Serious%20Business%20Man.mp3',
                title:"Boring, Serious Businessman",
                artist:'Stephen Gashler',
                rating:5,
                duration:'4:21'
            },
            {
                mp3:'/mp3/The%20Kingdom%20of%20the%20Dwarves.mp3',
                title:"The Kingdom of the Dwarves",
                artist:'Stephen Gashler',
                rating:5,
                duration:'3:31'
            },
            {
                mp3:'/mp3/Beulah%27s%20March.mp3',
                title:"Beulah's March",
                artist:'Stephen Gashler',
                rating:4,
                duration:'0:46'
            },
            {
                mp3:'/mp3/Buffalo%20Wings.mp3',
                title:"Buffalo Wings",
                artist:'Stephen Gashler',
                rating:4,
                duration:'2:49'
            },
            {
                mp3:'/mp3/Big%2C%20Big%20City.mp3',
                title:"Big, Big City",
                artist:'Stephen Gashler, William Whitlark',
                rating:4,
                duration:'4:33'
            },
            {
                mp3:'/mp3/Strolling%20through%20Town.mp3',
                title:"Strolling Through Town",
                artist:'Stephen Gashler',
                rating:4,
                duration:'1:36'
            },
            {
                mp3:'/mp3/Edward%27s%20Dream.mp3',
                title:"Edward's Dream",
                artist:'Stephen Gashler',
                rating:4,
                duration:'1:13'
            },
            {
                mp3:'/mp3/Bums.mp3',
                title:"Bums",
                artist:'Stephen Gashler',
                rating:3,
                duration:'4:03'
            },
            {
                mp3:'/mp3/Washing%20Machine.mp3',
                title:"Washing Machine",
                artist:'Stephen Gashler',
                rating:3,
                duration:'2:55'
            },
            {
                mp3:'/mp3/Forget%20About%20It.mp3',
                title:"Forget About It",
                artist:'Stephen Gashler',
                rating:3,
                duration:'2:56'
            },
            {
                mp3:'/mp3/Washing%20Machine%20Reprise.mp3',
                title:"Washing Machine Reprise",
                artist:'Stephen Gashler',
                rating:3,
                duration:'1:00'
            }
        ],
        'Rock': [
            {
                mp3:'/mp3/Clash.mp3',
                title:"Clash",
                artist:'Stephen Gashler',
                rating:5,
                duration:'5:35'
            },
            {
                mp3:'/mp3/I%27ve%20Got%20You.mp3',
                title:"I've Got You",
                artist:'Stephen Gashler',
                rating:5,
                duration:'3:13'
            },
            {
                mp3:'/mp3/We%20Are%20the%20Slain.mp3',
                title:"We Are the Slain",
                artist:'Stephen Gashler',
                rating:5,
                duration:'3:58'
            },
            {
                mp3:'/mp3/Pursuit.mp3',
                title:"Pursuit",
                artist:'Stephen Gashler, Monte Emerson',
                rating:5,
                duration:'4:55'
            },
            {
                mp3:'/mp3/Ride%2C%20Maiden%20Warriors.mp3',
                title:"Ride, Madien Warriors",
                artist:'Desire Gashler, Stephen Gashler',
                rating:4,
                duration:'3:10'
            },
            {
                mp3:'/mp3/Fallen.mp3',
                title:"Fallen",
                artist:'Desire Gashler, Stephen Gashler',
                rating:4,
                duration:'3:06'
            },
            {
                mp3:'/mp3/Love%2C%20Love%2C%20True%20Love.mp3',
                title:"Love, Love, True Love",
                artist:'Stephen Gashler',
                rating:4,
                duration:'3:02'
            },
            {
                mp3:'/mp3/Half-eaten%20Burger.mp3',
                title:"Half-eaten Burger",
                artist:'Stephen Gashler',
                rating:4,
                duration:'4:26'
            },
            {
                mp3:'/mp3/Let%27s%20Go%20Punch%20Some%20Dumb%20Guy.mp3',
                title:"Let's Go Raid the Saxons",
                artist:'Stephen Gashler',
                rating:3,
                duration:'1:17'
            },
            {
                mp3:'/mp3/I%20Like%20Myself.mp3',
                title:"I Like Myself",
                artist:'Stephen Gashler, William Whitlark',
                rating:3,
                duration:'2:04'
            },
        ],
        'Pop': [
            {
                mp3:'/mp3/Runner%20in%20the%20Rain.mp3',
                title:"Runner in the Rain",
                artist:'Teresa Gashler, Monte Emerson, Stephen Gashler',
                rating:5,
                duration:'3:51'
            },
            {
                mp3:'/mp3/Where%20Have%20You%20Gone.mp3',
                title:"Where Have You Gone",
                artist:'Stephen Gashler',
                rating:5,
                duration:'3:07'
            },
            {
                mp3:'/mp3/All%20Hallows%20Eve.mp3',
                title:"All Hallows Eve",
                artist:'Stephen Gashler, Curtis Wiederhold',
                rating:5,
                duration:'3:04'
            },
            {
                mp3:'/mp3/Where%20Do%20We%20Go%20From%20Here.mp3',
                title:"Where Do We Go From Here",
                artist:'Teresa Gashler, Stephen Gashler',
                rating:5,
                duration:'3:50'
            },
            {
                mp3:'/mp3/Stop%20Hiding%20Your%20Face.mp3',
                title:"Stop Hiding Your Face",
                artist:'Stephen Gashler, Curtis Wiederhold',
                rating:4,
                duration:'3:52'
            },
            {
                mp3:'/mp3/Finale.mp3',
                title:"Finale",
                artist:'Stephen Gashler',
                rating:4,
                duration:'3:36'
            },
            {
                mp3:'/mp3/Jane%20and%20Sue.mp3',
                title:"Jane and Sue",
                artist:'Stephen Gashler',
                rating:4,
                duration:'4:18'
            },
            {
                mp3:'/mp3/A%20New%20Life.mp3',
                title:"A New Life",
                artist:'Stephen Gashler',
                rating:3,
                duration:'3:47'
            },
            {
                mp3:'/mp3/I%27ve%20Got%20Magical%20Powers.mp3',
                title:"I've Got Magical Powers",
                artist:'Stephen Gashler',
                rating:3,
                duration:'2:08'
            },
        ],
        'World': [
            {
                mp3:'/mp3/Endless%20Night.mp3',
                title:"Endless Night",
                artist:'Stephen Gashler',
                rating:5,
                duration:'4:11'
            },
            {
                mp3:'/mp3/Interlude.mp3',
                title:"Interlude",
                artist:'Stephen Gashler',
                rating:5,
                duration:'1:13'
            },
            {
                mp3:'/mp3/Removed.mp3',
                title:"Removed",
                artist:'Stephen Gashler',
                rating:4,
                duration:'1:27'
            },
            {
                mp3:'/mp3/The%20Wisdom%20of%20Weck.mp3',
                title:"The Wisdom of Weck",
                artist:'Stephen Gashler',
                rating:4,
                duration:'2:36'
            },
            {
                mp3:'/mp3/Life%20is%20a%20Lie.mp3',
                title:"Life is a Lie",
                artist:'Teresa and Stephen Gashler',
                rating:3,
                duration:'2:35'
            },
            {
                mp3:'/mp3/We%27ll%20Take%20Your%20Treasure%20Away.mp3',
                title:"We'll Take Your Treasure Away",
                artist:'Stephen Gashler',
                rating:3,
                duration:'3:32'
            },
        ]
    };

    $scope.code = null;
    $scope.allowCoupon = true;
    $scope.price = price;
    $scope.playing = false;
    $scope.trackIndex = 0;
    $scope.trackCategory = 'Symphonic';
    $scope.track = $scope.tracks[$scope.trackCategory][$scope.trackIndex].mp3;

    // set audio
    $scope.setAudio = function()
    {
        $scope.track = $scope.tracks[$scope.trackCategory][$scope.trackIndex].mp3;
        if ($scope.audio !== undefined) {
            $scope.audio.pause();
        }
        $scope.audio = new Audio($scope.track);
        $scope.audio.play();
        $scope.track = $scope.tracks[$scope.trackCategory][$scope.trackIndex].buffering = $scope.audio.buffered.length;
        $scope.playing = true;
        $scope.audioInterval = $interval(function() {
            if ($scope.audio.ended) {
                $scope.setTrack();
                $interval.cancel($scope.audioInterval);
            }
        }, 100);
    }
    $scope.setAudio();

    // audio player
    $scope.setTrack = function(trackCategory, index, event)
    {
        if (trackCategory) {
            $scope.trackCategory = trackCategory;
            if ($(window).width() < 500) {
                $scope.scrollTo('#audioPlayer');
            }
        }
        if (index !== undefined) {
            if ($scope.trackIndex == index) {
                if (event !== undefined) {
                    if ($scope.playing) {
                        var width = $('.progress-container').outerWidth();
                        var percent = event.offsetX / width;
                        $scope.audio.currentTime = $scope.audio.duration * percent;
                        console.log({
                            'event.offsetX': event.offsetX,
                            'width': width,
                            'percent': percent,
                            '$scope.audio.currentTime': $scope.audio.currentTime
                        });
                        return;
                    } else {
                        return $scope.playPause();
                    }
                } else {
                    return $scope.playPause();
                }
            } else {
                $scope.trackIndex = index;
            }
        } else {
            if (trackCategory == undefined && $scope.tracks[$scope.trackCategory][($scope.trackIndex + 1)]) {
                $scope.trackIndex ++;
            } else {
                $scope.trackIndex = 0;
            }
        }
        $scope.setAudio();
    }

    $scope.playPause = function()
    {
        $scope.playing = !$scope.playing;
        if ($scope.playing) {
            $scope.audio.play();
        } else {
            $scope.audio.pause();
        }
    };


    // sizing
    function setHeight() {
        var height = $(window).height();
        $('#home-1, #home-3, #home-4').css('min-height', height + 'px');
    }
    setHeight();
    $(window).resize(function() {
        setHeight();
    });

    // filter for formatting currency without cents
    app.filter('noFractionCurrency',
    [ '$filter', '$locale',
    function(filter, locale) {
        var currencyFilter = filter('currency');
        var formats = locale.NUMBER_FORMATS;
        return function(amount, currencySymbol) {
            var value = currencyFilter(amount, currencySymbol);
            var sep = value.indexOf(formats.DECIMAL_SEP);
            if(amount >= 0) {
                return value.substring(0, sep);
            }
            return value.substring(0, sep) + ')';
        };
    } ]);

    // reveal popup
    // $scope.countdown = 60;
    // $scope.offerInterval = $interval(function() {
    //     if ($scope.countdown > 0) {
    //         $scope.countdown --;
    //     } else {
    //         $interval.cancel($scope.offerInterval);
    //         $scope.showPopup('#special-offer');
    //     }
    // }, 1000);

    // record minutes on site
    $scope.minutesInterval = $interval(function() {
        $scope.progress('minute')
    }, (60000));

    // record user progress
    $scope.progress = function(eventName)
    {
        $http.get('/progress/' + eventName);
    }

    // show popup
    $scope.showPopup = function(id) {
        $(id).css('visibility', 'visible').fadeIn();
    }

    // hide popup
    $scope.hidePopup = function(id) {
        $(id).fadeOut();
    }

    // cancel offer
    $scope.cancelOffer = function()
    {
        $interval.cancel($scope.interval);
    }

    // apply coupon
    $scope.applyCoupon = function(coupon) {
        if (coupon) {
            $scope.code = coupon;
        }
        $http.get('/apply-coupon/' + $scope.code).then(function(response) {
            $scope.price = response.data.price;
            $scope.coupon = response.data;
            if ($scope.price == 0) {
                window.location = '/download/QloXliONDJvTuarTgOgH';
            }
            if (coupon) {
                $scope.allowCoupon = false;
                $scope.hidePopup('#special-offer');
                $scope.scrollTo('#home-4-2');
            }
        });
    }

    // scroll to
    $scope.scrollTo = function(element)
    {
        $('html, body').animate({
            scrollTop: $(element).offset().top
        }, 2000);
    }
});
