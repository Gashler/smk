<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Progress;

class ProgressController extends Controller
{
    // store
    public function store($eventName)
    {
        // blacklist IP's used for testing
        $blacklist = [
            '45.56.25.234',
            '192.168.10.1',
            '75.169.237.70',
            '45.56.63.187'
        ];

        $ip = request()->ip();
        if (!in_array($ip, $blacklist)) {

            // record minutes on site
            if ($eventName == 'minute') {

                if ($progress = Progress::where([
                    'ip' => $ip,
                    'event' => $eventName
                ])->first()) {
                    $progress->update([
                        'value' => $progress->value + 1
                    ]);
                } else {
                    Progress::create([
                        'event' => $eventName,
                        'ip' => request()->ip(),
                        'value' => 1
                    ]);
                }

            // record all other progress
            } else {
                Progress::create([
                    'event' => $eventName,
                    'ip' => request()->ip()
                ]);
            }
        }
    }
}
